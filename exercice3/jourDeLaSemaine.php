<?php

  $jour = 4;

  echo "Version 1 : ";

  if ($jour == 1)
  {
    echo "Nous sommes lundi";
  }
  elseif ($jour == 2)
  {
    echo "Nous sommes mardi";
  }
  elseif ($jour == 3)
  {
    echo "Nous sommes mercredi";
  }
  elseif ($jour == 4)
  {
    echo "Nous sommes jeudi";
  }
  else
  {
  echo "Nous sommes ni lundi, ni mardi, ni mercredi, ni jeudi";
  }

  echo "<br /> <br /> Version 2 : "; //Retour à la ligne

  switch ($jour)
  {
    case 1:
      echo "Nous sommes lundi";
      break;
    case 2:
      echo "Nous sommes mardi";
      break;
    case 3:
      echo "Nous sommes mercredi";
        break;
    case 4:
      echo "Nous sommes jeudi";
        break;
    default:
      echo "Nous sommes ni lundi, ni mardi, ni mercredi, ni jeudi";
      break;
  }

?>
