<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title>Ajout contact</title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
  </head>
  <body>

  </body>
</html>


<?php

  //Check injection SQL
  //Vérifier d'où provient le formulaire
  //On part sur le principe qu'il peut y avoir 2 personnes qui possèdent le même nom
  //mais pas le même prénom donc : on check si le prenom + nom sont diff


  try
  {
    $pdo = new PDO("mysql:host=localhost;dbname=carnetdadresses", "root", "");
    $pdo->exec("SET CHARACTER SET utf8");
  }
  catch(Exception $e)
  {
    die('Erreur : ' . $e->getMessage());
    exit();
  }

  //Sécurisation des données envoyés par l'utilisateur

  if(isset($_POST['nom'])
  && isset($_POST['prenom'])
  && isset($_POST['adresse'])
  && isset($_POST['cp'])
  && isset($_POST['ville'])
  && isset($_POST['tel'])
  && !empty($_POST['nom'])
  && !empty($_POST['prenom'])
  && !empty($_POST['adresse'])
  && !empty($_POST['cp'])
  && !empty($_POST['ville'])
  && !empty($_POST['tel']))
  {
    if(!($_SERVER['HTTP_REFERER'] == 'http://localhost/Workspace_php/mysql/saisie.html'))
    {
      echo "Une erreur est survenue.";
      exit();
    }
    else
    {
      $nom = htmlentities(strtolower($_POST['nom']));
      $prenom = htmlentities(strtolower($_POST['prenom']));
      $adresse = htmlentities(strtolower($_POST['adresse']));
      $cp = htmlentities($_POST['cp']);
      $ville = htmlentities(strtolower($_POST['ville']));
      $tel = htmlentities($_POST['tel']);
    }
  }
  else
  {
    echo "Un des champs du formulaire est vide.";
    exit();
  }

  $tel = str_replace(".", "", $tel);

  if((is_numeric($cp) != 1) || (is_numeric($tel) != 1))
  {
    echo "Le champ 'Code postal' ou 'Téléphone' ne contient pas que des chiffres.";
    exit();
  }
  else
  {

      $req = $pdo->prepare('SELECT count(nom) FROM carnet WHERE nom = ? AND prenom = ?');
      $req->execute(array($nom, $prenom));

      while($data = $req->fetch())
      {
        $nombrePersonne = $data[0];
      }

      $req->closeCursor();

      if($nombrePersonne == 0)
      {
        $req = $pdo->prepare('INSERT INTO carnet(nom, prenom, adresse, code_postal, ville, telephone) VALUES(:nom, :prenom, :adresse, :code_postal, :ville, :telephone)');
        $req->execute(array('nom' => $nom,
                            'prenom' => $prenom,
                            'adresse' => $adresse,
                            'code_postal' => $cp,
                            'ville' => $ville,
                            'telephone' => $tel));
      }
      else
      {
        echo "Cette personne est déjà dans la base de données.";
        exit();
      }

      echo ucfirst($nom) . " " . ucfirst($prenom) . " a bien été ajouté dans la base de données.";
      echo "<p><a href='saisie.html'>Retour</a></p>";
  }
?>
