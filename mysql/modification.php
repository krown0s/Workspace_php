<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">

<?php

  if(!($_SERVER['HTTP_REFERER'] == 'http://localhost/Workspace_php/mysql/modifier.php' . '?id=' . $_POST['id']))
  {
    //echo "Une erreur est survenue.";
    header("Location: index.html");
    exit();
  }
  if(isset($_POST['nom']) && isset($_POST['prenom']) && isset($_POST['adresse']) && isset($_POST['ville']) && isset($_POST['cp']) && isset($_POST['tel']))
  {

      try
      {
        $pdo = new PDO("mysql:host=localhost;dbname=carnetdadresses", "root", "");
        $pdo->exec("SET CHARACTER SET utf8");
      }
      catch(Exception $e)
      {
        die('Erreur : ' . $e->getMessage());
        exit();
      }

      $nom = strtolower($_POST['nom']);
      $prenom = strtolower($_POST['prenom']);
      $adresse = strtolower($_POST['adresse']);
      $cp = $_POST['cp'];
      $ville = strtolower($_POST['ville']);
      $tel = $_POST['tel'];
      $id = $_POST['id'];

      $tel = str_replace(".", "", $tel);

      if((is_numeric($cp) != 1) || (is_numeric($tel) != 1))
      {
        echo "Le champ 'Code postal' ou 'Téléphone' ne contient pas que des chiffres.";
        exit();
      }

      if((is_numeric($id) != 1))
      {
        echo "Une erreur est survenue (2).";
        exit();
      }

      $req = $pdo->prepare('UPDATE carnet SET nom = :nom , prenom = :prenom , adresse = :adresse , code_postal = :code_postal , ville = :ville , telephone = :telephone WHERE id = :id');
      $req->execute(array(':nom' => $nom,
                          ':prenom' => $prenom,
                          ':adresse' => $adresse,
                          ':code_postal' => $cp,
                          ':ville' => $ville,
                          ':telephone' => $tel,
                          ':id' => $id));

      echo "Modification ok<br/>";
      echo "<a href='recherche.php'>Retour</a>";
      //header("location: recherche.php");

  }
  else
  {
    echo "Un des champs n'est pas valide.";
    exit();
  }

?>
